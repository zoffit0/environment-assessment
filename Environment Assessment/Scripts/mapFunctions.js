﻿function ShowMap() {
    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
}

function PutMarker(data) {
    var pm10sensor = data.data.sensors.find(sensor => (sensor.id == 88));

    var color = NaN;
    if (pm10sensor.value >= 50) {
        color = "red";
    }
    else if (pm10sensor.value >= 40 && pm10sensor.value < 50) {
        color = "orange";
    }
    else {
        color = "green";
    }

    var urlIcon = "https://leafletjs.com/SlavaUkraini/examples/custom-icons/leaf-" + color + ".png";

    var Icon = L.icon({
        iconUrl: urlIcon,
        shadowUrl: 'https://leafletjs.com/SlavaUkraini/examples/custom-icons/leaf-shadow.png',

        iconSize: [38, 95],
        shadowSize: [50, 64],
        iconAnchor: [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor: [-3, -76]
    });

    var lat = data.data.location.latitude;
    var long = data.data.location.longitude;

    var marker = L.marker([lat, long], { icon: Icon })
    marker.DeviceID = data.id
    marker.DeviceOwner = data.owner.username;

    marker.on('click', OnMarkerClick);

    map.addLayer(marker);

    return true;
}

function ShowDeviceData(data) {
    var sensorName = data.data.sensors[0].id
    var sensors = data.data.sensors.filter(element => document.getElementById("sensor" + element.id) !== null)

    sensors.forEach(element => document.getElementById("sensor" + element.id).innerHTML = element.value);
}

function ShowSensorData(data) {
    var orario = document.getElementById("selectOrario").value
    var valoriFiltrati = data.readings.filter(reading => reading[1] != 0);
    valoriFiltrati = valoriFiltrati.filter(reading => new Date(reading[0]).getHours() == orario - 1);

    var valori = valoriFiltrati.map(elem => elem[1]);
    var result = "-";

    if (!(valori.length === 0)) {
        if (data.function = "avg") {
            var avg = valori.reduce(function (p, c, i) { return p + (c - p) / (i + 1) }, 0);
            var decimalPlaces = 2;
            result = Number(Math.round(parseFloat(avg + 'e' + decimalPlaces)) + 'e-' + decimalPlaces).toFixed(decimalPlaces);
        }
        else if (data.function = "max") {
            result = valori.reduce(function (a, b) { return Math.max(a, b) }, 0);
        }
        else if (data.function = "min") {
            result = valori.reduce(function (a, b) { return Math.min(a, b) }, 0);
        }
    }

    var sensorId = data.sensor_id;

    document.getElementById("sensor" + sensorId).innerHTML = result;
}

function OnMarkerClick(e) {
    //alert(this.DeviceID);
    if (myChart != null) {
        myChart.destroy();
    }
    currentId = this.DeviceID;
    deviceOwner = this.DeviceOwner;
    document.getElementById("labelId").innerHTML = currentId;
    document.getElementById("labelOwner").innerHTML = deviceOwner;

    VisualizeDeviceData(this.DeviceID);
    console.log(this);
    console.log(e);
}

function AddMarker(id) {
    GetDeviceData(id);
}

function GetDeviceData(id) {
    url = "https://api.smartcitizen.me/v0/devices/" + id
    fetch(url).then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log(data);
        PutMarker(data);
    }).catch(function () {
        console.log("Errore richiesta API");
    });
}

function VisualizeDeviceData(id) {
    url = "https://api.smartcitizen.me/v0/devices/" + id
    fetch(url).then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log(data);
        ShowDeviceData(data);
    }).catch(function () {
        console.log("Errore richiesta API");
    });
}

function VisualizeSensorData(deviceId, sensorId, funzione, startDate, endDate) {

    url = "https://api.smartcitizen.me/v0/devices/" + deviceId + "/readings?sensor_id=" + sensorId + "&rollup=1h&from=" + startDate + "&to=" + endDate + "&function=" + funzione;
    fetch(url).then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log(data);
        ShowSensorData(data);
    }).catch(function () {
        console.log("Errore richiesta API");
    });
}

function CercaConParametri() {
    //map.clearLayers();
    //ShowMap();
    var funzione = document.getElementById("selectFunzioneVisualizzazione").value
    var giorni = document.getElementById("selectRangeData").value
    var orario = document.getElementById("selectOrario").value

    var today = new Date();
    var pastDate = new Date();

    pastDate.setDate(today.getDate() - giorni);
    pastDate.setHours(orario);
    pastDate.setMinutes(0);
    pastDate.setSeconds(0);
    pastDate.setMilliseconds(0);

    today.setHours(orario);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);

    var sensorsIdsList = [89, 87, 88, 56, 55, 58, 53, 14, 10];
    sensorsIdsList.forEach(sensId => {
        VisualizeSensorData(currentId, sensId, funzione, pastDate.toISOString(), today.toISOString());
    });
}

function VisualizeChart(labelsChart, dataChart, nomeSensore) {
    var ctx = document.getElementById("myChart");

    if (myChart != null) {
        myChart.destroy();
    }
    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labelsChart,
            datasets: [{
                label: nomeSensore,
                data: dataChart,
                backgroundColor: 'rgb(0, 155, 94)',
                borderWidth: 1
            }]
        },
        options: {
            responsive: false,
            scales: {
                xAxes: [{
                    ticks: {
                        maxRotation: 90,
                        minRotation: 80
                    },
                    gridLines: {
                        offsetGridLines: true // à rajouter
                    }
                },
                {
                    position: "top",
                    ticks: {
                        maxRotation: 90,
                        minRotation: 80
                    },
                    gridLines: {
                        offsetGridLines: true // et matcher pareil ici
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function ShowSensorDataChart(data, sensorName) {
    var sensorId = data.sensor_id;

    VisualizeChart(data.readings.map(elem => new Date(elem[0]).getDay().toString() + "/" + new Date(elem[0]).getMonth().toString() + "/" + new Date(elem[0]).getFullYear().toString()).reverse(), data.readings.map(elem => elem[1]).reverse(), sensorName);
}

function ShowSensorChart(sensorId, sensorName) {

    var funzione = document.getElementById("selectFunzioneVisualizzazione").value
    var giorni = document.getElementById("selectRangeData").value
    var orario = document.getElementById("selectOrario").value

    var today = new Date();
    var pastDate = new Date();

    pastDate.setDate(today.getDate() - giorni);
    pastDate.setHours(orario);
    pastDate.setMinutes(0);
    pastDate.setSeconds(0);
    pastDate.setMilliseconds(0);

    today.setHours(orario);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);

    VisualizeSensorDataChart(currentId, sensorId, sensorName, funzione, pastDate.toISOString(), today.toISOString());
}

function VisualizeSensorDataChart(deviceId, sensorId, sensorName, funzione, startDate, endDate) {

    url = "https://api.smartcitizen.me/v0/devices/" + deviceId + "/readings?sensor_id=" + sensorId + "&rollup=1h&from=" + startDate + "&to=" + endDate + "&function=" + funzione;
    fetch(url).then(function (response) {
        return response.json();
    }).then(function (data) {
        console.log(data);
        ShowSensorDataChart(data, sensorName);
    }).catch(function () {
        console.log("Errore richiesta API");
    });
}
