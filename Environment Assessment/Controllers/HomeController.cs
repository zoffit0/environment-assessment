﻿//using Environment_Assessment.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Environment_Assessment.Models;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;

namespace Environment_Assessment.Controllers
{
    public class HomeController : Controller
    {
        private int saltLengthLimit = 64;
        private const string tipoUtenteDefault = "Normale";
        private const string tipoUtenteAdmin = "Admin";
        private string connectionString = ConfigurationManager.AppSettings.Get("connectionStringControlloAmbientale");
        private string sessionKeyUsername = "username";
        private string sessionKeyIsLogged = "logged";
        private string sessionKeyIsAdmin = "Admin";

        public ActionResult Index()
        {


            List<int> sensorIds = GetDevicesIds();

            int zoom = 13;

            string jsonSensorsIDs = JsonConvert.SerializeObject(sensorIds);


            //RICORDA CSV
            string json = "var jsonSensorsIDs = " + "\" " + jsonSensorsIDs + " \"";
            ViewBag.coInit += Scriptify(json);
            return View();
        }

        public string Scriptify(string script)
        {
            return "<script>" + script + "</script>";
        }

        private List<int> GetDevicesIds()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = connection;
            sqlCommand.CommandText = "select DeviceID From Dispositivi WHERE Utente=@Utente";

            string utente = (string)Session[sessionKeyUsername];

            sqlCommand.Parameters.Add(new SqlParameter("@Utente", utente));

            List<int> devicesIds = new List<int>();

            try
            {
                connection.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    devicesIds.Add(dataReader.GetFieldValue<int>(0));
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                connection.Close();
            }

            return devicesIds;
        }

        public ActionResult About()
        {
            ViewBag.Message = "Controllo Ambientale";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpGet]
        public ActionResult Signup()
        {
            bool logged = false;

            if (Session[sessionKeyIsLogged] != null)
            {
                logged = (bool)Session[sessionKeyIsLogged];

                if (logged)
                {
                    return View("Profile");
                }
            }

            ViewBag.Message = "La tua pagina di registrazione.";

            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            bool logged = false;

            if (Session[sessionKeyIsLogged] != null)
            {
                logged = (bool)Session[sessionKeyIsLogged];

                if (logged)
                {
                    return View("Profile");
                }
            }

            ViewBag.Message = "La tua pagina di Accesso.";

            return View();
        }

        [HttpGet]
        public ActionResult Profile()
        {
            if (Session[sessionKeyIsLogged] == null)
            {
                return View("Login");
            }

            bool logged = false;
            logged = (bool)Session[sessionKeyIsLogged];

            if (!logged)
            {
                return View("Login");
            }

            ViewBag.Message = "Il tuo profilo";

            return View();
        }

        [HttpPost]
        public ActionResult VerificaCredenziali(Utente account)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = connection;
            sqlCommand.CommandText = "SELECT * FROM Utenti WHERE IndirizzoEmail=@IndirizzoEmail";
            sqlCommand.Parameters.Add(new SqlParameter("@IndirizzoEmail", account.IndirizzoEmail.Trim()));
            sqlCommand.Parameters.Add(new SqlParameter("@Password", account.Password.Trim()));

            try
            {
                connection.Open();
                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    string hashPassword = dataReader.GetFieldValue<string>(1);
                    string sale = dataReader.GetFieldValue<string>(2);
                    string tipoUtente = dataReader.GetFieldValue<string>(3);

                    if (GetHash(account.Password + sale) == hashPassword)
                    {
                        connection.Close();

                        Session[sessionKeyIsLogged] = true;
                        Session[sessionKeyUsername] = account.IndirizzoEmail;
                        Session[sessionKeyIsAdmin] = tipoUtente == tipoUtenteAdmin;

                        return View("Profile");
                    }


                }

                connection.Close();
                Session[sessionKeyIsLogged] = false;
                return View("Login");

            }
            catch (Exception e)
            {

            }
            finally
            {
                connection.Close();
            }

            return View();
        }

        [HttpPost]
        public ActionResult Esci()
        {
            Session[sessionKeyIsLogged] = false;
            Session[sessionKeyUsername] = string.Empty;
            Session[sessionKeyIsAdmin] = false;
            return View("Login");
        }

        [HttpPost]
        public ActionResult RegistraUtente(Utente utente)
        {
            string sale = GetSalt();
            string tipoUtente = tipoUtenteDefault;
            string passwordHash = GetHash(utente.Password.Trim() + sale);
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = connection;
            sqlCommand.CommandText = "INSERT INTO Utenti(IndirizzoEmail, Password, Sale, TipoUtente) VALUES(@IndirizzoEmail, @Password, @Sale, @TipoUtente)";
            sqlCommand.Parameters.Add(new SqlParameter("@IndirizzoEmail", utente.IndirizzoEmail.Trim()));
            sqlCommand.Parameters.Add(new SqlParameter("@Password", passwordHash));
            sqlCommand.Parameters.Add(new SqlParameter("@Sale", sale));
            sqlCommand.Parameters.Add(new SqlParameter("@TipoUtente", tipoUtente));

            try
            {
                connection.Open();
                if (sqlCommand.ExecuteNonQuery() > 0)
                {
                    connection.Close();
                    return View("Profile");
                }
                else
                {
                    connection.Close();
                    return View("Signup");
                }

            }
            catch (Exception e)
            {

            }
            finally
            {
                connection.Close();
            }

            return View("Signup");
        }

        private string GetSalt()
        {
            return GetSalt(saltLengthLimit);
        }
        private string GetSalt(int maximumSaltLength)
        {
            var salt = new byte[maximumSaltLength];
            using (var random = new RNGCryptoServiceProvider())
            {
                random.GetNonZeroBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }

        private string GetHash(string randomString)
        {
            var crypt = new System.Security.Cryptography.SHA256Managed();
            var hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

    }
}