﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Environment_Assessment.Models
{
    public class Utente
    {
        public String IndirizzoEmail {get; set; }

        public String Password { get; set; }

        public String Sale { get; set; }

        public String TipoUtente { get; set; }
    }
}